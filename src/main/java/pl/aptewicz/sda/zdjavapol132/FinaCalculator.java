package pl.aptewicz.sda.zdjavapol132;

import pl.aptewicz.sda.zdjavapol132.entity.SwimmingWR;

import java.math.BigDecimal;

public class FinaCalculator {

    /**
     * calculates FINA points for given time for 100 free male long course
     *
     * @param minutes  minutes
     * @param seconds  seconds
     * @param hundreds hundreds of seconds
     * @return FINA points 100 free male
     */
    public int calculate(Integer minutes, Integer seconds, Integer hundreds, SwimmingWR swimmingWR) {
        if (minutes == null || seconds == null || hundreds == null) {
            throw new UnsupportedOperationException("Time must be provided!");
        }
        if (minutes > 59 || minutes < 0) {
            throw new IllegalArgumentException("Minutes must be in <0; 59>");
        }
        if (seconds > 59 || seconds < 0) {
            throw new IllegalArgumentException("Seconds must be in <0; 59>");
        }
        if (hundreds > 99 || hundreds < 0) {
            throw new IllegalArgumentException("Hundreds must be in <0; 99>");
        }
        int minutesAsSeconds = minutes * 60;
        double hundredsAsSeconds = (double) hundreds / 100;
        double swimTimeAsSeconds = minutesAsSeconds + seconds + hundredsAsSeconds;
        double wrTimeAsSeconds = swimmingWR.asSeconds();
        return BigDecimal.valueOf(wrTimeAsSeconds / swimTimeAsSeconds).pow(3).multiply(BigDecimal.valueOf(1000))
                .intValue();
    }
}
