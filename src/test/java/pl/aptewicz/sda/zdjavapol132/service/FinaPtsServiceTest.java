package pl.aptewicz.sda.zdjavapol132.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.aptewicz.sda.zdjavapol132.FinaCalculator;
import pl.aptewicz.sda.zdjavapol132.entity.Gender;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingDistance;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingWR;
import pl.aptewicz.sda.zdjavapol132.repository.SwimmingWRsRepository;

import java.util.stream.Stream;

@ExtendWith(MockitoExtension.class)
public class FinaPtsServiceTest {

    @Mock
    private SwimmingWRsRepository repository;

    @ParameterizedTest
    @MethodSource("testDataForFinaPtsService")
    public void shouldCalculateCorrectFinaPts(int minutes, int seconds, int hundreds, SwimmingDistance swimmingDistance,
            Gender gender, int expectedFinaPts, SwimmingWR wr) {
        // given
        FinaCalculator finaCalculator = new FinaCalculator();

        Mockito.when(repository.findWRByDistanceAndGender(swimmingDistance, gender)).thenReturn(wr);

        FinaPtsService finaPtsService = new FinaPtsService(repository, finaCalculator);

        // when
        int finaPts = finaPtsService.calculateFinaPtsForDistanceAndGender(minutes, seconds, hundreds, swimmingDistance,
                gender);

        // then
        Mockito.verify(repository, Mockito.times(1)).findWRByDistanceAndGender(swimmingDistance, gender);
        Mockito.verify(repository, Mockito.never()).findFastestWR();
        Assertions.assertEquals(expectedFinaPts, finaPts);
        Mockito.verifyNoMoreInteractions(repository);
    }

    private static Stream<Arguments> testDataForFinaPtsService() {
        // @formatter:off
        return Stream.of(
                Arguments.of(4, 16, 76, SwimmingDistance.FREESTYLE_400_LC, Gender.MALE, 629, new SwimmingWR(3, 40, 7, SwimmingDistance.FREESTYLE_400_LC, Gender.MALE)),
                Arguments.of(4, 7, 53, SwimmingDistance.FREESTYLE_400_SC, Gender.MALE, 630, new SwimmingWR(3, 32, 25, SwimmingDistance.FREESTYLE_400_SC, Gender.MALE)),
                Arguments.of(2, 20, 44, SwimmingDistance.BREASTSTROKE_200_SC, Gender.MALE, 626, new SwimmingWR(2, 0, 16, SwimmingDistance.BREASTSTROKE_200_SC, Gender.MALE)),
                Arguments.of(4, 38, 7, SwimmingDistance.INDIVIDUAL_MEDLEY_400_SC, Gender.MALE, 602, new SwimmingWR(3, 54, 81, SwimmingDistance.INDIVIDUAL_MEDLEY_400_SC, Gender.MALE))
        );
        // @formatter:on
    }

    @Test
    public void shouldThrowCalculationNotPossibleExceptionWhenWRNotAvailableInRepository() {
        // given
        FinaPtsService service = new FinaPtsService(repository, new FinaCalculator());

        // when
        Mockito.when(repository.findWRByDistanceAndGender(Mockito.any(), Mockito.any()))
                .thenThrow(new IllegalStateException());
        Executable executable = () -> service.calculateFinaPtsForDistanceAndGender(1, 15, 15,
                SwimmingDistance.FREESTYLE_100_LC, Gender.FEMALE);

        // then
        Assertions.assertThrows(CalculationNotPossibleException.class, executable);
    }
}
