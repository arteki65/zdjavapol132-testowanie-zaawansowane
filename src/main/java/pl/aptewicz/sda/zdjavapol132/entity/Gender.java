package pl.aptewicz.sda.zdjavapol132.entity;

public enum Gender {
    MALE, FEMALE
}
