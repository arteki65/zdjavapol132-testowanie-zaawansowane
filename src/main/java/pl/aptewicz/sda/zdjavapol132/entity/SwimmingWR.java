package pl.aptewicz.sda.zdjavapol132.entity;

import java.math.BigDecimal;

public class SwimmingWR {

    private final int minutes;

    private final int seconds;

    private final int hundreds;

    private final SwimmingDistance distance;

    private final Gender gender;

    public SwimmingWR(int minutes, int seconds, int hundreds, SwimmingDistance distance, Gender gender) {
        this.minutes = minutes;
        this.seconds = seconds;
        this.hundreds = hundreds;
        this.distance = distance;
        this.gender = gender;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public int getHundreds() {
        return hundreds;
    }

    public SwimmingDistance getDistance() {
        return distance;
    }

    public Gender getGender() {
        return gender;
    }

    public double asSeconds() {
        return BigDecimal.valueOf(minutes * 60L).add(BigDecimal.valueOf(seconds))
                .add(BigDecimal.valueOf((double) hundreds / 100)).doubleValue();
    }

    @Override
    public String toString() {
        return "SwimmingWR{" + "minutes=" + minutes + ", seconds=" + seconds + ", hundreds=" + hundreds + ", distance="
                + distance + '}';
    }
}
