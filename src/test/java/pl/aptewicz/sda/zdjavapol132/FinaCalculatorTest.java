package pl.aptewicz.sda.zdjavapol132;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import pl.aptewicz.sda.zdjavapol132.entity.Gender;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingDistance;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingWR;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class FinaCalculatorTest {

    @ParameterizedTest
    @MethodSource("testDataForFinaCalculator")
    @CsvFileSource(resources = "/finaCalculatorTestData.csv", delimiter = ',')
    public void shouldCalculateCorrectFinaPtsForGivenTime(Integer minutes, Integer seconds, Integer hundreds,
            Integer expectedFinaPts) {
        // given
        FinaCalculator finaCalculator = new FinaCalculator();

        // when
        int finaPts = finaCalculator.calculate(minutes, seconds, hundreds,
                new SwimmingWR(0, 46, 86, SwimmingDistance.FREESTYLE_100_LC, Gender.MALE));

        // then
        Assertions.assertEquals(expectedFinaPts, finaPts);
    }

    private static Stream<Arguments> testDataForFinaCalculator() {
        // @formatter:off
        return Stream.of(
                Arguments.of(0, 46, 86, 1000),
                Arguments.of(0, 55, 55, 600),
                Arguments.of(1, 55, 55, 66),
                Arguments.of(1, 30, 0, 141)
        );
        // @formatter:on
    }

    @ParameterizedTest
    @MethodSource("testDataForFinaCalculatorWithExceptions")
    public void shouldThrowUnsupportedOperationExceptionWhenAtLeastOnePartOfTimeIsNull(Integer minutes, Integer seconds,
            Integer hundreds, Class<Throwable> exceptionClass, String errorMsg) {
        // given
        FinaCalculator finaCalculator = new FinaCalculator();

        // when
        ThrowableAssert.ThrowingCallable executable = () -> finaCalculator.calculate(minutes, seconds, hundreds,
                new SwimmingWR(0, 46, 86, SwimmingDistance.FREESTYLE_100_LC, Gender.MALE));

        // then
        assertThatExceptionOfType(exceptionClass).isThrownBy(executable).withMessage(errorMsg);
    }

    private static Stream<Arguments> testDataForFinaCalculatorWithExceptions() {
        // @formatter:off
        return Stream.of(
                Arguments.of(null, null, null, UnsupportedOperationException.class, "Time must be provided!"),
                Arguments.of(60, 0, 0, IllegalArgumentException.class, "Minutes must be in <0; 59>"),
                Arguments.of(0, 60, 0, IllegalArgumentException.class, "Seconds must be in <0; 59>"),
                Arguments.of(0, 0, 210, IllegalArgumentException.class, "Hundreds must be in <0; 99>")
        );
        // @formatter:on
    }
}
