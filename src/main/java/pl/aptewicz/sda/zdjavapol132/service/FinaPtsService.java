package pl.aptewicz.sda.zdjavapol132.service;

import pl.aptewicz.sda.zdjavapol132.FinaCalculator;
import pl.aptewicz.sda.zdjavapol132.entity.Gender;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingDistance;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingWR;
import pl.aptewicz.sda.zdjavapol132.repository.SwimmingWRsRepository;

public class FinaPtsService {

    private final SwimmingWRsRepository swimmingWRsRepository;

    private final FinaCalculator finaCalculator;

    public FinaPtsService(SwimmingWRsRepository swimmingWRsRepository, FinaCalculator finaCalculator) {
        this.swimmingWRsRepository = swimmingWRsRepository;
        this.finaCalculator = finaCalculator;
    }

    public int calculateFinaPtsForDistanceAndGender(int minutes, int seconds, int hundreds,
            SwimmingDistance swimmingDistance, Gender gender) {
        try {
            SwimmingWR wr = swimmingWRsRepository.findWRByDistanceAndGender(swimmingDistance, gender);
            return finaCalculator.calculate(minutes, seconds, hundreds, wr);
        } catch (IllegalStateException e) {
            throw new CalculationNotPossibleException();
        }
    }
}
