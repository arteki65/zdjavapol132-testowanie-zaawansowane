package pl.aptewicz.sda.zdjavapol132.repository;

import pl.aptewicz.sda.zdjavapol132.entity.Gender;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingDistance;
import pl.aptewicz.sda.zdjavapol132.entity.SwimmingWR;

public interface SwimmingWRsRepository {

    SwimmingWR findWRByDistanceAndGender(SwimmingDistance distance, Gender gender);

    SwimmingWR findFastestWR();
}
